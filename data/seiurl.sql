-- auto-generated definition
create table URLS
(
    ID           LONG auto_increment,
    SHORTLINK        VARCHAR                             not null,
    NAME_PATH    VARCHAR,
    ORIGINAL_URL VARCHAR                             not null,
    CREATED_AT   TIMESTAMP default CURRENT_TIMESTAMP not null,
    UPDATED_AT   TIMESTAMP default CURRENT_TIMESTAMP not null,
    ACCESSED     INT       default 0                 not null,
    EXPIRED_AT   TIMESTAMP,
    constraint URLS_PK
        primary key (ID)
);

create unique index URLS_URL_UINDEX
    on URLS (shortlink);

