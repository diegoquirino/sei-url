# SEI-URL

O projeto SEI-URL (do inglês Service of Easy Identifiable URL) é uma API Rest (tecnologia _backend_) que habilita aos seus clientes as seguintes funcionalidades:

1. Cadastro, listagem, edição e exclusão de URLs (com a disponibilização de _shortlink_ a partir de uma combinação aleatória de palavras (em inglês) OU um nome/path fornecido pelo usuário;
2. Redirecionamento para a URL original, a partir do _shortlink_;
3. Contador de acessos aos links; e,
4. Realizar a expiração de um link (a partir de sua identificação única).

Com isto, os clientes podem implementar suas próprias regras já suportadas pelo _backend_ para, por exemplo:

- Registrar as visitas que um link já teve;
- Expirar links com base no tempo de vida;
- Expirar links com base no número de visitas; e
- Uma interface para gerenciar os links.

## Realizando a configuração inicial

### Pré-requisitos

Para que você possa executar o projeto em seu servidor, é necessário ter **instalado** os seguintes _softwares_:

- [Java 11 ou posterior](https://www.oracle.com/java/technologies/downloads/)
- [Maven 3+](https://maven.apache.org/download.cgi)

### Tecnologias utilizadas

Este projeto foi realizado com a composição das seguintes _frameworks_:

- [Servidor Java SpringBoot v2.6.4](https://start.spring.io/)
- [Banco de dados (em arquivo) H2](https://www.baeldung.com/spring-boot-h2-database)
- [Java Faker Library, para geração dos shortlinks aleatórios](https://github.com/DiUS/java-faker)
- [Springfox Swagger API/UI, para geração automatizada do site da documentação HTML](https://springfox.github.io/springfox/docs/current/)

## Procedimentos para instalação

1. Realize o [download do código-fonte do projeto.](https://gitlab.com/diegoquirino/sei-url.git)
2. Descompacte no seu servidor e certifique-se de que nenhum processo esteja em execução na porta 8080;
3. Verifique que no [diretório de dados](./data) existe um banco H2 previamente disponível (arquivo [seiurl.mv.db](./data/seiurl.mv.db));
4. Na raiz do diretório, em seu _terminal/prompt de comando_ realize o teste, a partir da execução do comando `mvnw clean test`, para se certificar que o _software_ irá executar adequadamente em seu servidor; e,
5. Se tudo ocorrer bem (sucesso), implante o servidor, a partir da execução do comando `mvnw clean spring-boot:run`;

# Utilização

## Gerenciar URLs encurtadas:

Pronto! Agora que o servidor está "no ar", poderá usar os métodos da API para a realização da gestão de URLs e também do acesso de URLs encurtadas.

- [Consulte a documentação gerada automaticamente pelo Swagger para saber quais _endpoints_ estão disponíveis](http://localhost:8080/swagger-ui/#/)
- [Utilize o Postman para realizar chamadas aos _endpoints_ do projeto e poder gerenciar suas URLs](https://www.postman.com/downloads/)

## Padrão de mensagens de erro:

Lembre-se que o idioma da implementação e mensagens que porventura sejam vistas, estarão em inglês.

Caso ocorra alguma situação anómala OU alguma restrição de negócio seja detetada, uma mensagem no formato abaixo será apresentada (por exemplo):

```json
{
   "timestamp":"2022-03-08T02:54:42.817155",
   "error":"error.data.url.does.not.exists",
   "errors":[
      "Url with shortlink hamster-april-schauer does not exist!"
   ]
}
```

## Para acessar a URL encurtada, basta inserir o caminho:

No _browser_ da sua preferência, basta acessar o _shortlink_ gerado para ter acesso ao redirecionamento para a URL (por exemplo):

```
<seu-servidor>/seiurl/<nome do path (informado OU aleatoriamente gerado)>
Ex: http://localhost:8080/seiurl/google, http://localhost:8080/seiurl/, etc.
```

# Contato e dúvidas

* Carlos Diego Quirino Lima
* **Email**: diegoquirino@gmail.com
* **Zap**: +55 (83) 98818-1876
