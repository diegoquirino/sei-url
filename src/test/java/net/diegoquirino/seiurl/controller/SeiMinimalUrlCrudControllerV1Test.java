package net.diegoquirino.seiurl.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.diegoquirino.seiurl.model.Url;
import net.diegoquirino.seiurl.repository.UrlRepository;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.TimeZone;

import static org.hamcrest.Matchers.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@DisplayName("Validate UrlCrud API v1")
class SeiMinimalUrlCrudControllerV1Test {

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected UrlRepository urlRepository;

    private final String URL_CRUD_API_V1_URI = "/seiurl/api/v1/urls";
    private final String EXPIRE = "/expire";

    private final String ISO8601_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    private final SimpleDateFormat SDF = new SimpleDateFormat(ISO8601_FORMAT);
    private final TimeZone UTC = TimeZone.getTimeZone("UTC");

    @Nested
    @DisplayName("when creating an Url")
    class WhenCreatingUrlTest {
        Url fixedUrl = createFixedUrl("");
        Url createdUrl;
        @BeforeEach
        void setup() {
            urlRepository.save(fixedUrl);
        }
        @AfterEach
        void tearDown() {
            if (createdUrl!=null) {
                urlRepository.delete(createdUrl);
            }
            urlRepository.delete(fixedUrl);
        }
        @Test
        @DisplayName("with name/path and original url")
        void withNamePathAndOriginalUrl() throws Exception {
            String responseJsonString = mockMvc.perform(
                            post(URL_CRUD_API_V1_URI)
                                    .contentType(APPLICATION_JSON_VALUE)
                                    .content(readJson("post_valid_url_with_name_path.json")))
                    .andExpect(status().isCreated())
                    .andDo(print())
                    .andReturn().getResponse().getContentAsString();
            createdUrl = new ObjectMapper().readValue(responseJsonString, Url.class);
        }
        @Test
        @DisplayName("without name/path and original url")
        void withoutNamePathAndOriginalUrl() throws Exception {
            String responseJsonString = mockMvc.perform(
                            post(URL_CRUD_API_V1_URI)
                                    .contentType(APPLICATION_JSON_VALUE)
                                    .content(readJson("post_valid_url_without_name_path.json")))
                    .andExpect(status().isCreated())
                    .andDo(print())
                    .andReturn().getResponse().getContentAsString();
            createdUrl = new ObjectMapper().readValue(responseJsonString, Url.class);
        }
        @Test
        @DisplayName("with name/path that already exists")
        void withNamePathThatAlreadyExists() throws Exception {
            mockMvc.perform(
                            post(URL_CRUD_API_V1_URI)
                                    .contentType(APPLICATION_JSON_VALUE)
                                    .content(readJson("post_valid_url_with_name_path_already_exists.json")))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.error", equalTo("error.data.shortlink.already.exists")))
                    .andExpect(jsonPath("$.errors[0]", startsWith("It is not possible to save Url with a SHORTLINK that already exists")))
                    .andDo(print());
        }
        @Test
        @DisplayName("without original url")
        void withoutOriginalUrl() throws Exception {
            mockMvc.perform(
                            post(URL_CRUD_API_V1_URI)
                                    .contentType(APPLICATION_JSON_VALUE)
                                    .content(readJson("post_invalid_url_without_original_url.json")))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.error", equalTo("error.validation")))
                    .andExpect(jsonPath("$.errors[0]", equalTo("OriginalUrl field obligatory!")))
                    .andDo(print());
        }
        @Test
        @DisplayName("with empty body json")
        void withEmptyBodyJson() throws Exception {
            mockMvc.perform(
                            post(URL_CRUD_API_V1_URI)
                                    .contentType(APPLICATION_JSON_VALUE)
                                    .content("{}"))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.error", equalTo("error.validation")))
                    .andExpect(jsonPath("$.errors[0]", equalTo("OriginalUrl field obligatory!")))
                    .andDo(print());
        }
        @Test
        @DisplayName("with invalid empty body json")
        void withInvalidEmptyBodyJson() throws Exception {
            mockMvc.perform(
                            post(URL_CRUD_API_V1_URI)
                                    .contentType(APPLICATION_JSON_VALUE)
                                    .content(""))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.error", equalTo("error.validation")))
                    .andExpect(jsonPath("$.errors[0]", startsWith("Invalid format of json file: You must have at least {}")))
                    .andDo(print());
        }
        @Test
        @DisplayName("without http(s) in the original url")
        void withoutHttpsInOriginalUrl() throws Exception {
            mockMvc.perform(
                            post(URL_CRUD_API_V1_URI)
                                    .contentType(APPLICATION_JSON_VALUE)
                                    .content(readJson("post_invalid_url_without_http_at_beginning.json")))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.error", equalTo("error.validation")))
                    .andExpect(jsonPath("$.errors[0]", equalTo("OriginalUrl must be complete and well-formed (including 'http' or 'https' at beginning)!")))
                    .andDo(print());
        }
    }

    @Nested
    @DisplayName("when reading")
    class WhenReadingUrlsTest {
        final int QUANTITY = 10;
        ArrayList<Url> urls = new ArrayList<>();
        @BeforeEach
        void setup() {
            for(int i = 0; i < QUANTITY; i++)
                urls.add(urlRepository.save(createFixedUrl("" + i)));
        }
        @AfterEach
        void tearDown() {
            urlRepository.deleteAll(urls);
        }
        @Test
        @DisplayName("a not empty list of Urls")
        void aNotEmptyListOfUrls() throws Exception {
            mockMvc.perform(
                            get(URL_CRUD_API_V1_URI)
                                    .contentType(APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", hasSize(greaterThanOrEqualTo(QUANTITY))))
                    .andDo(print());
        }
        @Test
        @DisplayName("an empty list of Urls")
        void anEmptyListOfUrls() throws Exception {
            urlRepository.deleteAll();
            mockMvc.perform(
                            get(URL_CRUD_API_V1_URI)
                                    .contentType(APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", hasSize(0)))
                    .andDo(print());
        }
        @Test
        @DisplayName("a Url that was previously saved")
        void aUrlThatWasPreviouslySaved() throws Exception {
            mockMvc.perform(
                            get(URL_CRUD_API_V1_URI + "/" + urls.get(0).getId())
                                    .contentType(APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", equalTo(urls.get(0).getId().intValue())))
                    .andExpect(jsonPath("$.shortlink", equalTo(urls.get(0).getShortlink())))
                    .andExpect(jsonPath("$.originalUrl", equalTo(urls.get(0).getOriginalUrl())))
                    .andDo(print());
        }
        @Test
        @DisplayName("a Url that not exists")
        void aUrlThatNotExists() throws Exception {
            mockMvc.perform(
                            get(URL_CRUD_API_V1_URI + "/0")
                                    .contentType(APPLICATION_JSON_VALUE))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.error", equalTo("error.data.url.does.not.exists")))
                    .andExpect(jsonPath("$.errors[0]", equalTo("Url id 0 does not exists!")))
                    .andDo(print());
        }
        @Test
        @DisplayName("a Url that not exists with an invalid id")
        void aUrlThatNotExistsWithAnInvalidId() throws Exception {
            mockMvc.perform(
                            get(URL_CRUD_API_V1_URI + "/asdfg")
                                    .contentType(APPLICATION_JSON_VALUE))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.error", equalTo("error.data.url.does.not.exists")))
                    .andExpect(jsonPath("$.errors[0]", startsWith("Url does not exists! Wrong parameter conversion")))
                    .andDo(print());
        }
    }

    @Nested
    @DisplayName("when updating an Url")
    class WhenUpdatingAnUrlTest {
        Url fixedUrl = createFixedUrl("");
        Url fixedUrl1 = createFixedUrl("1");
        @BeforeEach
        void setup() {
            urlRepository.save(fixedUrl);
            urlRepository.save(fixedUrl1);
        }
        @AfterEach
        void tearDown() {
            urlRepository.deleteById(fixedUrl.getId());
            urlRepository.deleteById(fixedUrl1.getId());
        }
        @Test
        @DisplayName("with valid data")
        void withValidData() throws Exception {
            SDF.setTimeZone(UTC);
            String updateAt = addChar(SDF.format(fixedUrl.getUpdatedAt()), ':', 26);
            mockMvc.perform(
                            put(URL_CRUD_API_V1_URI + "/" + fixedUrl.getId())
                                    .contentType(APPLICATION_JSON_VALUE)
                                    .content(readJson("put_valid_url.json")))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", equalTo(fixedUrl.getId().intValue())))
                    .andExpect(jsonPath("$.shortlink", not(equalTo(fixedUrl.getShortlink()))))
                    .andExpect(jsonPath("$.originalUrl", not(equalTo(fixedUrl.getOriginalUrl()))))
                    .andExpect(jsonPath("$.namePath", not(equalTo(fixedUrl.getNamePath()))))
                    .andExpect(jsonPath("$.updatedAt", not(equalTo(updateAt))))
                    .andDo(print());
        }
        @Test
        @DisplayName("with name-path that is equal to a shortlink already exists")
        void withNamePathThatIsEqualToAShortlinkAlreadyExists() throws Exception {
            mockMvc.perform(
                            put(URL_CRUD_API_V1_URI + "/" + fixedUrl.getId())
                                    .contentType(APPLICATION_JSON_VALUE)
                                    .content(readJson("put_name_path_already_exists.json")))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.error", equalTo("error.data.shortlink.already.exists")))
                    .andExpect(jsonPath("$.errors[0]", startsWith("It is not possible to save Url with a SHORTLINK that already exists")))
                    .andDo(print());
        }
        @Test
        @DisplayName("with shortlink that already exists")
        void withShortlinkThatAlreadyExists() throws Exception {
            mockMvc.perform(
                            put(URL_CRUD_API_V1_URI + "/" + fixedUrl.getId())
                                    .contentType(APPLICATION_JSON_VALUE)
                                    .content(readJson("put_shortlink_already_exists.json")))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.error", equalTo("error.data.shortlink.already.exists")))
                    .andExpect(jsonPath("$.errors[0]", startsWith("It is not possible to save Url with a SHORTLINK that already exists")))
                    .andDo(print());
        }
        @Test
        @DisplayName("that not exists")
        void aUrlThatNotExists() throws Exception {
            mockMvc.perform(
                            put(URL_CRUD_API_V1_URI + "/0")
                                    .contentType(APPLICATION_JSON_VALUE)
                                    .content(readJson("put_valid_url.json")))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.error", equalTo("error.data.url.does.not.exists")))
                    .andExpect(jsonPath("$.errors[0]", equalTo("Url id 0 does not exists!")))
                    .andDo(print());
        }
        @Test
        @DisplayName("that not exists with an invalid id")
        void aUrlThatNotExistsWithAnInvalidId() throws Exception {
            mockMvc.perform(
                            put(URL_CRUD_API_V1_URI + "/asdfg")
                                    .contentType(APPLICATION_JSON_VALUE)
                                    .content(readJson("put_valid_url.json")))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.error", equalTo("error.data.url.does.not.exists")))
                    .andExpect(jsonPath("$.errors[0]", startsWith("Url does not exists! Wrong parameter conversion")))
                    .andDo(print());
        }
        @Test
        @DisplayName("with empty body json")
        void withEmptyBodyJson() throws Exception {
            SDF.setTimeZone(UTC);
            String updateAt = addChar(SDF.format(fixedUrl.getUpdatedAt()), ':', 26);
            mockMvc.perform(
                            put(URL_CRUD_API_V1_URI + "/" + fixedUrl.getId())
                                    .contentType(APPLICATION_JSON_VALUE)
                                    .content("{}"))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", equalTo(fixedUrl.getId().intValue())))
                    .andExpect(jsonPath("$.shortlink", equalTo(fixedUrl.getShortlink())))
                    .andExpect(jsonPath("$.originalUrl", equalTo(fixedUrl.getOriginalUrl())))
                    .andExpect(jsonPath("$.namePath", equalTo(fixedUrl.getNamePath())))
                    .andExpect(jsonPath("$.updatedAt", equalTo(updateAt)))
                    .andDo(print());
        }
        @Test
        @DisplayName("with invalid empty body json")
        void withInvalidEmptyBodyJson() throws Exception {
            mockMvc.perform(
                            put(URL_CRUD_API_V1_URI + "/" + fixedUrl.getId())
                                    .contentType(APPLICATION_JSON_VALUE)
                                    .content(""))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.error", equalTo("error.validation")))
                    .andExpect(jsonPath("$.errors[0]", startsWith("Invalid format of json file: You must have at least {}")))
                    .andDo(print());
        }
        @Test
        @DisplayName("without http(s) in the original url")
        void withoutHttpsInOriginalUrl() throws Exception {
            mockMvc.perform(
                            put(URL_CRUD_API_V1_URI + "/" + fixedUrl.getId())
                                    .contentType(APPLICATION_JSON_VALUE)
                                    .content(readJson("post_invalid_url_without_http_at_beginning.json")))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.error", equalTo("error.validation")))
                    .andExpect(jsonPath("$.errors[0]", equalTo("OriginalUrl must be complete and well-formed (including 'http' or 'https' at beginning)!")))
                    .andDo(print());
        }
    }

    @Nested
    @DisplayName("when deleting an Url")
    class WhenDeletingAnUrlTest {
        Url fixedUrl = createFixedUrl("");
        @BeforeEach
        void setup() {
            urlRepository.save(fixedUrl);
        }
        @AfterEach
        void tearDown() {
            urlRepository.delete(fixedUrl);
        }
        @Test
        @DisplayName("that exists")
        void thatExists() throws Exception {
            mockMvc.perform(
                            delete(URL_CRUD_API_V1_URI + "/" + fixedUrl.getId())
                                    .contentType(APPLICATION_JSON_VALUE))
                    .andExpect(status().isNoContent())
                    .andDo(print());
        }
        @Test
        @DisplayName("that not exists")
        void aUrlThatNotExists() throws Exception {
            mockMvc.perform(
                            delete(URL_CRUD_API_V1_URI + "/0")
                                    .contentType(APPLICATION_JSON_VALUE))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.error", equalTo("error.data.url.does.not.exists")))
                    .andExpect(jsonPath("$.errors[0]", equalTo("Url id 0 does not exists!")))
                    .andDo(print());
        }
        @Test
        @DisplayName("that not exists with an invalid id")
        void aUrlThatNotExistsWithAnInvalidId() throws Exception {
            mockMvc.perform(
                            delete(URL_CRUD_API_V1_URI + "/asdfg")
                                    .contentType(APPLICATION_JSON_VALUE))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.error", equalTo("error.data.url.does.not.exists")))
                    .andExpect(jsonPath("$.errors[0]", startsWith("Url does not exists! Wrong parameter conversion")))
                    .andDo(print());
        }
    }

    @Nested
    @DisplayName("when expiring an Url")
    class WhenExpiringAnUrlTest {
        Url fixedUrl = createFixedUrl("");
        Url expiredUrl = createFixedUrl("1");
        @BeforeEach
        void setup() {
            urlRepository.save(fixedUrl);
            expiredUrl.setExpiredAt(Timestamp.valueOf(LocalDateTime.now()));
            urlRepository.save(expiredUrl);
        }
        @AfterEach
        void tearDown() {
            urlRepository.delete(fixedUrl);
            urlRepository.delete(expiredUrl);
        }
        @Test
        @DisplayName("that exists")
        void thatExists() throws Exception {
            mockMvc.perform(
                            patch(URL_CRUD_API_V1_URI + "/" + fixedUrl.getId() + EXPIRE)
                                    .contentType(APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", equalTo(fixedUrl.getId().intValue())))
                    .andExpect(jsonPath("$.shortlink", equalTo(fixedUrl.getShortlink())))
                    .andExpect(jsonPath("$.originalUrl", equalTo(fixedUrl.getOriginalUrl())))
                    .andExpect(jsonPath("$.namePath", equalTo(fixedUrl.getNamePath())))
                    .andExpect(jsonPath("$.expiredAt", not(equalTo(null))))
                    .andDo(print());
        }
        @Test
        @DisplayName("that not exists")
        void aUrlThatNotExists() throws Exception {
            mockMvc.perform(
                            patch(URL_CRUD_API_V1_URI + "/0" + EXPIRE)
                                    .contentType(APPLICATION_JSON_VALUE))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.error", equalTo("error.data.url.does.not.exists")))
                    .andExpect(jsonPath("$.errors[0]", equalTo("Url id 0 does not exists!")))
                    .andDo(print());
        }
        @Test
        @DisplayName("that not exists with an invalid id")
        void aUrlThatNotExistsWithAnInvalidId() throws Exception {
            mockMvc.perform(
                            patch(URL_CRUD_API_V1_URI + "/asdfg" + EXPIRE)
                                    .contentType(APPLICATION_JSON_VALUE))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.error", equalTo("error.data.url.does.not.exists")))
                    .andExpect(jsonPath("$.errors[0]", startsWith("Url does not exists! Wrong parameter conversion")))
                    .andDo(print());
        }
        @Test
        @DisplayName("already expired")
        void aUrlAlreadyExpired() throws Exception {
            mockMvc.perform(
                            patch(URL_CRUD_API_V1_URI + "/" + expiredUrl.getId() + EXPIRE)
                                    .contentType(APPLICATION_JSON_VALUE))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.error", equalTo("error.data.url.is.expired")))
                    .andExpect(jsonPath("$.errors[0]", equalTo("Url id " + expiredUrl.getId() + " is expired!")))
                    .andDo(print());
        }
    }

    public static String readJson(String file) throws Exception {
        return new String(Files.readAllBytes(Paths.get(ClassLoader.getSystemResource(file).toURI())));
    }

    private Url createFixedUrl(String sufix) {
        sufix = sufix != null && !sufix.isEmpty() ? "-" + sufix : "";
        Url url = new Url();
        url.setNamePath("criado-fixo" + sufix);
        url.setShortlink("criado-fixo" + sufix);
        url.setOriginalUrl("https://www.criadofixo" + sufix + ".com.br");
        Timestamp time = Timestamp.valueOf(LocalDateTime.now());
        url.setCreatedAt(time);
        url.setUpdatedAt(time);
        url.setAccessed(0);
        return url;
    }

    public String addChar(String str, char ch, int position) {
        StringBuilder sb = new StringBuilder(str);
        sb.insert(position, ch);
        return sb.toString();
    }

}