package net.diegoquirino.seiurl;

import net.diegoquirino.seiurl.config.SeiUrlConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({
		SeiUrlConfiguration.class
})
public class SeiUrlApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeiUrlApplication.class, args);
	}

}
