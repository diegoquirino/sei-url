package net.diegoquirino.seiurl.exception;

import java.time.LocalDateTime;
import java.util.List;

public class UrlDoesNotExistsException extends RuntimeException{
    public static final String EXCEPTION_CODE = "error.data.url.does.not.exists";

    private final transient Problem problem;

    public UrlDoesNotExistsException(List<String> errors) {
        problem = new Problem(LocalDateTime.now(), EXCEPTION_CODE, errors);
    }

    public Problem getProblem() {
        return problem;
    }
}
