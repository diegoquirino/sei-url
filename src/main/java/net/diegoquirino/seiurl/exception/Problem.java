package net.diegoquirino.seiurl.exception;

import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
/**
 * This class defines system-wide error response pattern
 */
public class Problem implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Error time
     */
    private LocalDateTime timestamp;
    /**
     * Error code
     */
    private String error;
    /**
     * Errors list
     */
    private List<String> errors;

}
