package net.diegoquirino.seiurl.exception;

import java.time.LocalDateTime;
import java.util.List;

public class UrlShortlinkAlreadyExistsException extends RuntimeException{
    public static final String EXCEPTION_CODE = "error.data.shortlink.already.exists";

    private final transient Problem problem;

    public UrlShortlinkAlreadyExistsException(List<String> errors) {
        problem = new Problem(LocalDateTime.now(), EXCEPTION_CODE, errors);
    }

    public Problem getProblem() {
        return problem;
    }
}
