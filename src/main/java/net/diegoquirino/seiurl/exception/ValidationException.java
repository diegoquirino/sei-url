package net.diegoquirino.seiurl.exception;

import java.time.LocalDateTime;
import java.util.List;

/**
 * This class manages all system-wide validation exceptions
 * defining "error.validation" as prefix
 */
public class ValidationException extends RuntimeException {

    public static final String EXCEPTION_CODE = "error.validation";

    private final transient Problem problem;

    public ValidationException(List<String> errors) {
        problem = new Problem(LocalDateTime.now(), EXCEPTION_CODE, errors);
    }

    public Problem getProblem() {
        return problem;
    }

}
