package net.diegoquirino.seiurl.exception;

import java.time.LocalDateTime;
import java.util.List;

public class UrlIsExpiredException extends RuntimeException{
    public static final String EXCEPTION_CODE = "error.data.url.is.expired";

    private final transient Problem problem;

    public UrlIsExpiredException(List<String> errors) {
        problem = new Problem(LocalDateTime.now(), EXCEPTION_CODE, errors);
    }

    public Problem getProblem() {
        return problem;
    }
}
