package net.diegoquirino.seiurl.model.dto;

import lombok.Data;

import javax.validation.constraints.Pattern;

@Data
public class UrlUpdateV1Dto {
    private String namePath;
    private String shortlink;
    @Pattern(
            regexp = "https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)",
            message = "OriginalUrl must be complete and well-formed (including 'http' or 'https' at beginning)!"
    )
    private String originalUrl;
}
