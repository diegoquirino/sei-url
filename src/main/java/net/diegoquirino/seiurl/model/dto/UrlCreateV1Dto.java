package net.diegoquirino.seiurl.model.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Data
public class UrlCreateV1Dto {
    private String namePath;
    @NotEmpty(message = "OriginalUrl field obligatory!")
    @Pattern(
            regexp = "https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)",
            message = "OriginalUrl must be complete and well-formed (including 'http' or 'https' at beginning)!"
    )
    private String originalUrl;
}
