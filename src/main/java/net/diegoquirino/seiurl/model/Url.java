package net.diegoquirino.seiurl.model;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "URLS")
@Data
public class Url {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    private String shortlink;
    private String namePath;
    private String originalUrl;
    private Integer accessed;
    private Timestamp createdAt;
    private Timestamp updatedAt;
    private Timestamp expiredAt;

}
