package net.diegoquirino.seiurl.controller;

import net.diegoquirino.seiurl.exception.UrlDoesNotExistsException;
import net.diegoquirino.seiurl.exception.UrlIsExpiredException;
import net.diegoquirino.seiurl.exception.UrlShortlinkAlreadyExistsException;
import net.diegoquirino.seiurl.model.Url;
import net.diegoquirino.seiurl.model.dto.UrlCreateV1Dto;
import net.diegoquirino.seiurl.model.dto.UrlUpdateV1Dto;
import net.diegoquirino.seiurl.service.UrlService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(
        value = "/seiurl/api/v1/urls",
        produces = MediaType.APPLICATION_JSON_VALUE
)
public class SeiUrlCrudControllerV1 extends BaseController {

    static final Logger LOGGER = LogManager.getLogger(SeiUrlCrudControllerV1.class);

    @Autowired
    private UrlService urlService;

    @PostMapping
    public ResponseEntity<Object> create(
            @Valid @RequestBody UrlCreateV1Dto urlCreateV1Dto,
            @ApiIgnore Errors errors
    ) {
        if(errors.hasErrors()) {
            return validationErrorResponse(errors);
        }
        LOGGER.info("Creating Url with data: {}", urlCreateV1Dto);
        Url url;
        try {
            url = urlService.create(urlCreateV1Dto);
        } catch (UrlShortlinkAlreadyExistsException e) {
            return new ResponseEntity<>(e.getProblem(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(url, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<Object> readAll() {
        LOGGER.info("Reading all saved Urls");
        List<Url> urls = urlService.readAll();
        return new ResponseEntity<>(urls, HttpStatus.OK);
    }

    @GetMapping(value = "/{urlId}")
    public ResponseEntity<Object> read(@PathVariable Long urlId) {
        LOGGER.info("Reading Url with id: {}", urlId);
        Url url;
        try {
            url = urlService.read(urlId);
        } catch (UrlDoesNotExistsException e) {
            return new ResponseEntity<>(e.getProblem(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(url, HttpStatus.OK);
    }

    @PutMapping(value = "/{urlId}")
    public ResponseEntity<Object> update(
            @PathVariable Long urlId,
            @Valid @RequestBody UrlUpdateV1Dto urlUpdateV1Dto,
            @ApiIgnore Errors errors
    ) {
        if(errors.hasErrors()) {
            return validationErrorResponse(errors);
        }
        LOGGER.info("Updating Url id '{}' Url with data: {}", urlId, urlUpdateV1Dto);
        Url url;
        try {
            url = urlService.update(urlId, urlUpdateV1Dto);
        } catch (UrlShortlinkAlreadyExistsException e) {
            return new ResponseEntity<>(e.getProblem(), HttpStatus.BAD_REQUEST);
        } catch (UrlDoesNotExistsException e) {
            return new ResponseEntity<>(e.getProblem(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(url, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{urlId}")
    public ResponseEntity<Object> delete(@PathVariable Long urlId) {
        LOGGER.info("Deleting Url with id: {}", urlId);
        try {
            urlService.delete(urlId);
        } catch (UrlDoesNotExistsException e) {
            return new ResponseEntity<>(e.getProblem(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping(value = "/{urlId}/expire")
    public ResponseEntity<Object> expire(@PathVariable Long urlId) {
        LOGGER.info("Expiring Url with id: {}", urlId);
        Url url;
        try {
            url = urlService.expire(urlId);
        } catch (UrlDoesNotExistsException e) {
            return new ResponseEntity<>(e.getProblem(), HttpStatus.BAD_REQUEST);
        } catch (UrlIsExpiredException e) {
            return new ResponseEntity<>(e.getProblem(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(url, HttpStatus.OK);
    }

}
