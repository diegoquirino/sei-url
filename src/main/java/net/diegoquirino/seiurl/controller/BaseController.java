package net.diegoquirino.seiurl.controller;

import net.diegoquirino.seiurl.exception.UrlDoesNotExistsException;
import net.diegoquirino.seiurl.exception.ValidationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class BaseController {

    public HttpHeaders getHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Access-Control-Allow-Methods", "OPTIONS, HEAD, GET, POST, PUT, PATCH");
        headers.add("Access-Control-Allow-Headers", "Content-Type");
        return headers;
    }

    public ResponseEntity<Object> validationErrorResponse(Errors errors) {
        return new ResponseEntity<>(
                new ValidationException(errors.getAllErrors().stream()
                        .map(ObjectError::getDefaultMessage).collect(Collectors.toList())).getProblem(),
                getHttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<Object> handleTypeMismatch(MethodArgumentTypeMismatchException e) {
        ArrayList<String> errors = new ArrayList<>();
        errors.add("Url does not exists! Wrong parameter conversion: " + e.getMessage());
        return new ResponseEntity<>(
                new UrlDoesNotExistsException(errors).getProblem(),
                getHttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<Object> handleNotReadable(HttpMessageNotReadableException e) {
        ArrayList<String> errors = new ArrayList<>();
        errors.add("Invalid format of json file: You must have at least {}. " + e.getMessage());
        return new ResponseEntity<>(
                new ValidationException(errors).getProblem(),
                getHttpHeaders(), HttpStatus.BAD_REQUEST);
    }

}
