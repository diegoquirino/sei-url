package net.diegoquirino.seiurl.controller;

import net.diegoquirino.seiurl.exception.UrlDoesNotExistsException;
import net.diegoquirino.seiurl.exception.UrlIsExpiredException;
import net.diegoquirino.seiurl.model.Url;
import net.diegoquirino.seiurl.service.UrlRedirectService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping(
        value = "/seiurl",
        produces = MediaType.APPLICATION_JSON_VALUE
)
public class SeiUrlRedirectControllerV1 {

    static final Logger LOGGER = LogManager.getLogger(SeiUrlRedirectControllerV1.class);

    @Autowired
    private UrlRedirectService urlRedirectService;

    @GetMapping(value = "/{shortlink}")
    public ResponseEntity<Object> getOriginalUrl(@PathVariable String shortlink) throws URISyntaxException {
        LOGGER.info("Opening URL /seiurl/{}", shortlink);
        Url url;
        try {
            url = urlRedirectService.getOriginalUrl(shortlink);
        } catch (UrlDoesNotExistsException e) {
            return new ResponseEntity<>(e.getProblem(), HttpStatus.BAD_REQUEST);
        } catch (UrlIsExpiredException e) {
            return new ResponseEntity<>(e.getProblem(), HttpStatus.BAD_REQUEST);
        }
        URI uri = new URI(url.getOriginalUrl());
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(uri);
        return new ResponseEntity<>(httpHeaders, HttpStatus.SEE_OTHER);
    }

}
