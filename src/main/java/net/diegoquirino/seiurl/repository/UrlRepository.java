package net.diegoquirino.seiurl.repository;

import net.diegoquirino.seiurl.model.Url;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UrlRepository extends CrudRepository<Url, Long> {
    Optional<Url> getByShortlink(String shortlink);
}
