package net.diegoquirino.seiurl.config;

import net.diegoquirino.seiurl.controller.SeiUrlCrudControllerV1;
import net.diegoquirino.seiurl.controller.SeiUrlRedirectControllerV1;
import net.diegoquirino.seiurl.service.UrlRedirectImpl;
import net.diegoquirino.seiurl.service.UrlServiceImpl;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({
        SeiUrlCrudControllerV1.class,
        SeiUrlRedirectControllerV1.class,
        UrlServiceImpl.class,
        UrlRedirectImpl.class
})
public class SeiUrlConfiguration {
}
