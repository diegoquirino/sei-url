package net.diegoquirino.seiurl.service;

import com.github.javafaker.Faker;
import net.diegoquirino.seiurl.exception.UrlIsExpiredException;
import net.diegoquirino.seiurl.exception.UrlDoesNotExistsException;
import net.diegoquirino.seiurl.exception.UrlShortlinkAlreadyExistsException;
import net.diegoquirino.seiurl.model.Url;
import net.diegoquirino.seiurl.model.dto.UrlCreateV1Dto;
import net.diegoquirino.seiurl.model.dto.UrlUpdateV1Dto;
import net.diegoquirino.seiurl.repository.UrlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UrlServiceImpl implements UrlService {

    @Autowired
    private UrlRepository urlRepository;

    @Override
    public Url create(UrlCreateV1Dto urlCreateV1Dto) {
        Url url = new Url();
        url.setNamePath(urlCreateV1Dto.getNamePath());
        url.setOriginalUrl(urlCreateV1Dto.getOriginalUrl());
        Timestamp time = Timestamp.valueOf(LocalDateTime.now());
        url.setCreatedAt(time);
        url.setUpdatedAt(time);
        url.setAccessed(0);

        if (url.getNamePath() == null) {
            url.setShortlink(
                    lowerWithoutSpecialChars(Faker.instance().animal().name()) + "-" +
                    lowerWithoutSpecialChars(Faker.instance().funnyName().name())
            );
        } else {
            url.setShortlink(lowerWithoutSpecialChars(url.getNamePath()));
        }

        try {
            url = urlRepository.save(url);
        } catch (DataIntegrityViolationException e) {
            ArrayList<String> errors = new ArrayList<>();
            errors.add("It is not possible to save Url with a SHORTLINK that already exists: " + url.getShortlink());
            throw new UrlShortlinkAlreadyExistsException(errors);
        }

        return url;
    }

    @Override
    public List<Url> readAll() {
        List<Url> urls = new ArrayList<>();
        for (Url url : urlRepository.findAll()) {
            urls.add(url);
        }
        return urls;
    }

    @Override
    public Url read(Long id) {
        Optional<Url> url = urlRepository.findById(id);
        if(url.isPresent()) {
            return url.get();
        } else {
            ArrayList<String> errors = new ArrayList<>();
            errors.add("Url id " + id + " does not exists!");
            throw new UrlDoesNotExistsException(errors);
        }
    }

    @Override
    public Url update(Long id, UrlUpdateV1Dto urlUpdateV1Dto) {
        Optional<Url> url = urlRepository.findById(id);
        if(url.isPresent()) {
            Url presentUrl = url.get();
            if(urlUpdateV1Dto.getNamePath() == null
                    && urlUpdateV1Dto.getOriginalUrl() == null
                    && urlUpdateV1Dto.getShortlink() == null
            ) {
                return presentUrl;
            }
            presentUrl.setShortlink(urlUpdateV1Dto.getShortlink());
            presentUrl.setNamePath(urlUpdateV1Dto.getNamePath());
            presentUrl.setOriginalUrl(urlUpdateV1Dto.getOriginalUrl());
            if (presentUrl.getNamePath() == null) {
                presentUrl.setShortlink(
                        lowerWithoutSpecialChars(Faker.instance().animal().name()) + "-" +
                                lowerWithoutSpecialChars(Faker.instance().funnyName().name())
                );
            } else {
                presentUrl.setShortlink(lowerWithoutSpecialChars(url.get().getNamePath()));
            }
            presentUrl.setUpdatedAt(Timestamp.valueOf(LocalDateTime.now()));
            try {
                urlRepository.save(presentUrl);
            } catch (DataIntegrityViolationException e) {
                ArrayList<String> errors = new ArrayList<>();
                errors.add("It is not possible to save Url with a SHORTLINK that already exists: " + url.get().getShortlink());
                throw new UrlShortlinkAlreadyExistsException(errors);
            }
            return presentUrl;
        } else {
            ArrayList<String> errors = new ArrayList<>();
            errors.add("Url id " + id + " does not exists!");
            throw new UrlDoesNotExistsException(errors);
        }
    }

    @Override
    public void delete(Long id) {
        Optional<Url> url = urlRepository.findById(id);
        if(url.isPresent()) {
            urlRepository.delete(url.get());
        } else {
            ArrayList<String> errors = new ArrayList<>();
            errors.add("Url id " + id + " does not exists!");
            throw new UrlDoesNotExistsException(errors);
        }
    }

    @Override
    public Url expire(Long id) {
        Optional<Url> url = urlRepository.findById(id);
        if(url.isPresent()) {
            Url presentUrl = url.get();
            if ( presentUrl.getExpiredAt() == null ) {
                presentUrl.setExpiredAt(Timestamp.valueOf(LocalDateTime.now()));
            } else {
                ArrayList<String> errors = new ArrayList<>();
                errors.add("Url id " + id + " is expired!");
                throw new UrlIsExpiredException(errors);
            }
            return urlRepository.save(presentUrl);
        } else {
            ArrayList<String> errors = new ArrayList<>();
            errors.add("Url id " + id + " does not exists!");
            throw new UrlDoesNotExistsException(errors);
        }
    }

    private String lowerWithoutSpecialChars(String str) {
        return str.toLowerCase().trim().replaceAll("\\s+","-").replaceAll("[^a-zA-Z0-9\\-]","");
    }

}
