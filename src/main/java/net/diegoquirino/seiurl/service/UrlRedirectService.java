package net.diegoquirino.seiurl.service;

import net.diegoquirino.seiurl.model.Url;
import org.springframework.stereotype.Service;

@Service
public interface UrlRedirectService {
    Url getOriginalUrl(String shortlink);
}
