package net.diegoquirino.seiurl.service;

import net.diegoquirino.seiurl.exception.UrlDoesNotExistsException;
import net.diegoquirino.seiurl.exception.UrlIsExpiredException;
import net.diegoquirino.seiurl.model.Url;
import net.diegoquirino.seiurl.repository.UrlRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Optional;

public class UrlRedirectImpl implements UrlRedirectService {

    @Autowired
    private UrlRepository urlRepository;

    @Override
    public Url getOriginalUrl(String shortlink) {
        Optional<Url> url = urlRepository.getByShortlink(shortlink);
        if(url.isPresent()) {
            Url presentUrl = url.get();
            if(presentUrl.getExpiredAt()!=null) {
                ArrayList<String> errors = new ArrayList<>();
                errors.add("Url with shortlink " + shortlink + " is expired!");
                throw new UrlIsExpiredException(errors);
            }
            presentUrl.setAccessed(presentUrl.getAccessed() + 1);
            urlRepository.save(presentUrl);
            return presentUrl;
        } else {
            ArrayList<String> errors = new ArrayList<>();
            errors.add("Url with shortlink " + shortlink + " does not exist!");
            throw new UrlDoesNotExistsException(errors);
        }
    }
}
