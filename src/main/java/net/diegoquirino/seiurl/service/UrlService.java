package net.diegoquirino.seiurl.service;

import net.diegoquirino.seiurl.model.Url;
import net.diegoquirino.seiurl.model.dto.UrlCreateV1Dto;
import net.diegoquirino.seiurl.model.dto.UrlUpdateV1Dto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UrlService {
    Url create(UrlCreateV1Dto urlCreateV1Dto);
    List<Url> readAll();
    Url read(Long id);
    Url update(Long id, UrlUpdateV1Dto urlUpdateV1Dto);
    void delete(Long id);
    Url expire(Long id);
}
